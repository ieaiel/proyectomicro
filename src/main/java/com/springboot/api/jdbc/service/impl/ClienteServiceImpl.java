package com.springboot.api.jdbc.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springboot.api.jdbc.dao.impl.ClienteDaoImpl;
import com.springboot.api.jdbc.model.Cliente;
import com.springboot.api.jdbc.model.Respuesta;
import com.springboot.api.jdbc.service.ClienteService;

@Service
public class ClienteServiceImpl implements ClienteService{
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private ClienteDaoImpl _clientelDao;
	
	@Override
	public Respuesta getAllClientes() {
		
		return _clientelDao.getAllClientes();
	}

	@Override
	public Respuesta getCliente(Integer id) {
		
		return _clientelDao.getCliente(id);
	}

	@Override
	public Respuesta saveCliente(Cliente cliente) {
		/*try {
			_clientelDao.saveCliente(cliente);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}*/
		return _clientelDao.saveCliente(cliente);
	}

	@Override
	public Respuesta deleteCliente(Integer id) {
		/*try {
			_clientelDao.deleteCliente(id);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}*/
		return _clientelDao.deleteCliente(id);
	}

}
