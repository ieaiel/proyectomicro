package com.springboot.api.jdbc.service;

import java.util.List;

import com.springboot.api.jdbc.model.Cliente;
import com.springboot.api.jdbc.model.Respuesta;

public interface ClienteService {
	
	Respuesta getAllClientes();
	Respuesta getCliente(Integer id);
	Respuesta saveCliente(Cliente cliente);
	Respuesta deleteCliente(Integer id);
	
}
