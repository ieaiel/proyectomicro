package com.springboot.api.jdbc.dao.impl;

import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import com.springboot.api.jdbc.dao.ClienteDao;
import com.springboot.api.jdbc.model.Cliente;
import com.springboot.api.jdbc.model.Respuesta;
import com.springboot.api.jdbc.rowmapper.ClienteRowMapper;

@Repository
public class ClienteDaoImpl extends JdbcDaoSupport implements ClienteDao {

	public ClienteDaoImpl(DataSource dataSource) {
		this.setDataSource(dataSource);
	}
	
	@Override
	public Respuesta getAllClientes() {
		
		List<Cliente> listaClientes = new ArrayList<Cliente>();
		Respuesta miRespuesta = new Respuesta();
		
		String sql = " SELECT id, nombres, apellido_pat, apellido_mat, sexo, direccion, estado\n" + 
				" FROM microservicios.cliente";
		
		try {
			
			RowMapper<Cliente> clienteRow = new ClienteRowMapper();
			listaClientes = getJdbcTemplate().query(sql, clienteRow);
			logger.debug("Se han listado "+listaClientes.size()+" clientes");
					
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		
		//return listaClientes;
		miRespuesta.setClientes(listaClientes);
		miRespuesta.setCodigo_respuesta("0000");
		
		return miRespuesta;
		
	}

	@Override
	public Respuesta getCliente(Integer id) {
		Cliente cliente = new Cliente();	
		List<Cliente> listaClientes = new ArrayList<Cliente>();
		Respuesta miRespuesta = new Respuesta();
		
		String sql = " SELECT id, nombres, apellido_pat, apellido_mat, sexo, direccion, estado\n" + 
				" FROM microservicios.cliente where id='"+id+"'";
				
		try {
			
			RowMapper<Cliente> personaRow = new ClienteRowMapper();
			listaClientes = getJdbcTemplate().query(sql, personaRow);
			
			cliente = listaClientes.get(0);
			
			logger.debug("Se ha traido a la persona "+listaClientes.get(0).toString());
			
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		
		miRespuesta.setCliente(listaClientes.get(0));
		miRespuesta.setCodigo_respuesta("0000");
		
		return miRespuesta;
	}

	@Override
	public Respuesta saveCliente(Cliente cliente) {
		
		String sql = "insert into microservicios.cliente (nombres, apellido_pat, apellido_mat, sexo, direccion, estado) "  
				+ "values (?, ?, ?, ?, ?, ?);";
		Respuesta miRespuesta = new Respuesta();
		Object[] params = { cliente.getNombres(), cliente.getApellido_pat(), cliente.getApellido_mat(), cliente.getSexo(), cliente.getDireccion(), cliente.getEstado()};
		int[] tipos = { Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR};
		
		try {
			
			int filas = getJdbcTemplate().update(sql, params,tipos);
			
			logger.debug("Se han insertado : "+filas+" filas");
			logger.debug("Se ha registrado al cliente "+cliente.toString());
			
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		miRespuesta.setDescripcion("Cliente registrado con éxito");
		miRespuesta.setCodigo_respuesta("0000");
		return miRespuesta;
	}

	@Override
	public Respuesta deleteCliente(Integer id) {
		int regeliminados = 0;		
		Respuesta miRespuesta = new Respuesta();
		String sql = " delete from microservicios.cliente where id ='"+id+"'";		
		try {			
			regeliminados = getJdbcTemplate().update(sql);
			logger.debug("Se han eliminado "+regeliminados+" cliente con id = "+id);
		} catch (Exception e) {			
			logger.error(e.getMessage());
		}
		
		miRespuesta.setDescripcion("Cliente eliminado con éxito");
		miRespuesta.setCodigo_respuesta("0000");
		return miRespuesta;
	}

}
