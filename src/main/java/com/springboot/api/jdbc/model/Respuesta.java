package com.springboot.api.jdbc.model;

import java.util.List;

public class Respuesta {

	String codigo_respuesta;
	String descripcion;
	Cliente cliente;
	List<Cliente> clientes;
	
	public String getCodigo_respuesta() {
		return codigo_respuesta;
	}
	public void setCodigo_respuesta(String codigo_respuesta) {
		this.codigo_respuesta = codigo_respuesta;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public List<Cliente> getClientes() {
		return clientes;
	}
	public void setClientes(List<Cliente> clientes) {
		this.clientes = clientes;
	}
	
	
}
